package sender;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Created by elby on 7/30/16.
 */

import receiver.SchemaEntry;
public class Connection implements Runnable{

    public long currentChunk = 0;
    public long ackChunk = -1;
    public boolean lastAck = false;
    public InetAddress address;
    @Override
    public void run() {
        ArrayList<SchemaEntry> entries = Sender.entries;
        byte[] ack = new byte[8];
        DatagramPacket packet;
        Thread listener = null;

        try {
            for(SchemaEntry e : entries){

                System.out.println("Sending " + e.file + " to " + address.getHostAddress());
                FileInputStream fis = new FileInputStream(new File(e.file));
                while(currentChunk <= e.chunks)
                {
                    //gotta get that last chunk
                    byte[] buffer = new byte[Sender.length];
                    if(currentChunk == e.chunks) buffer = new byte[e.lastChunk];

                    fis.read(buffer);
//                    printBuffer(buffer);
                    byte[] payload = makePayload(currentChunk, buffer);
                    packet = new DatagramPacket(payload, payload.length, address, 6669);

                    //Spawn ack reception thread
                    listener = new Thread(new Listener(this, currentChunk));
                    listener.start();

                    System.out.println(address + " chunk: " + currentChunk + "/" + e.chunks);

                    while(listener.isAlive()){
                        //Add probability code here
                        if(!SchemaEntry.dropPacket(Sender.prob)){
                            Sender.send(packet);
                            // System.out.println(currentChunk);
                        }
                    }
                    currentChunk++;
                }

                currentChunk = 0;
                setAckChunk(-1);
                fis.close();
            }
            lastAck = true;
            listener.interrupt();
            listener.join();
            System.out.println(address + " has recieved all files");

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private byte[] makePayload(long seq, byte[] buffer){
        int i;
        byte[] bSeq = SchemaEntry.longToBytes(seq);
        byte[] payload = new byte[buffer.length + 8];

        for(i = 0; i < 8; i++){
            payload[i] = bSeq[i];
        }

        for(i = 8; i < payload.length; i++){
            payload[i] = buffer[i - 8];
        }

        return payload;
    }

    static void printBuffer(byte[] bytes){
        for(int i = 0; i < bytes.length; i++){
            System.out.print(bytes[i] + ",");
        }
        System.out.println();
    }

    public synchronized void setAckChunk(long l){
        this.ackChunk = l;
    }


    public synchronized long getAckChunk(){
        return this.ackChunk;
    }}


