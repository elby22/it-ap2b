package sender;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import receiver.SchemaEntry;
public class Sender {

    static int length = 256;
    static public ArrayList<SchemaEntry> entries;
    static final int SCHEMA_MAX = 500;
    static public byte[] schemaPayload;
    static public int prob = 0;
    static public DatagramSocket sendSocket;
    static public MulticastSocket broadcast;
    static private DatagramPacket schemaPacket;
    static private InetAddress hostName;


//    private boolean initialized

    static public Map<InetAddress, Connection> connections;

    public static void main(String[] args) throws IOException {

        connections = new HashMap<>();
        hostName = InetAddress.getLocalHost();
        System.out.println("Broadcasting from " + hostName.getHostAddress());

        //Get all files in directory
        File folder = new File(args[0]);
        prob = Integer.parseInt(args[1]);
        // File folder = new File("/home/egb37/Downloads/test/");

        File[] listOfFiles = folder.listFiles();

        entries = new ArrayList<>();

        String schema = "";

        //send each file name in list to chunkFile

        schema = hostName.getHostAddress() + ";";
        for (File file : listOfFiles) {
            if (file.isFile()) {
                String filename = file.getName();
                long chunks = file.length() / length;
                int lastChunk = (int)(file.length() - (chunks * length));

                entries.add(new SchemaEntry(file.getAbsolutePath(), chunks, lastChunk));

                String fileSchema = filename + ";" + chunks + ";" + lastChunk + ";";

                schema += fileSchema;
            }
        }
        
        System.out.println(schema);
        schemaPayload = new byte[SCHEMA_MAX];
        byte[] bSchema = schema.getBytes();

        for(int i = 0; i < bSchema.length; i++){
            schemaPayload[i] = bSchema[i];
        }

        System.out.println("Payload bytes: " + schemaPayload.length);

        //Spawn listener here
        new Thread(new Router()).start();
        System.out.println("Sending schema");

        //Used for unicast
        sendSocket = new DatagramSocket(6666);

        broadcast = new MulticastSocket(5555);

        InetAddress group = InetAddress.getByName("225.226.227.228");
        schemaPacket = new DatagramPacket(Sender.schemaPayload, Sender.SCHEMA_MAX, group, 6669);

        while(true){
            broadcast.send(schemaPacket);
        }

    }

    public static synchronized void send(DatagramPacket packet) throws IOException {
        sendSocket.send(packet);
    }
}
