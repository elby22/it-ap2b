package sender;

import receiver.SchemaEntry;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

/**
 * Created by elby on 7/30/16.
 */
public class Listener implements Runnable{

    private Connection connection;
    private long chunk; //sequence number
    @Override
    public void run() {

        while(chunk != connection.getAckChunk()){
           // System.out.println("Listener: " + chunk + "/" + connection.getAckChunk());
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public Listener(Connection c, long chunk){
        this.chunk = chunk;
        this.connection = c;
    }

}