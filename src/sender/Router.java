package sender;

import receiver.SchemaEntry;

import java.io.IOException;
import java.net.*;

/**
 * Created by elby on 8/8/16.
 */
public class Router implements Runnable{


    @Override
    public void run() {

        try {

            byte[] payload = new byte[12];
            DatagramSocket ackSocket = new DatagramSocket(6668); //receiving ack on 6668
            DatagramPacket packet = new DatagramPacket(payload, payload.length);

            do{
                ackSocket.receive(packet);
                route(payload);
            }while(true);

//            ackSocket.close();

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void route(byte[] payload) throws UnknownHostException {

        byte[] bAddress = new byte[4];
        byte[] bAckChunk = new byte[8];

        for(int i = 0; i < 4; i++){
            bAddress[i] = payload[i];
        }

        for(int i = 0; i < 8; i++){
            bAckChunk[i] = payload[i + 4];
        }

        long ackChunk = SchemaEntry.bytesToLong(bAckChunk);
        InetAddress address = InetAddress.getByAddress(bAddress);

        if(ackChunk == -1) {
            if (!Sender.connections.containsKey(address)) {
                Connection c = new Connection();
                c.address = address;

                Sender.connections.put(address, c);

                new Thread(c).start();
                System.out.println("New connection from " + address);
            }
        }else{
            Connection c = Sender.connections.get(address);
            if(c != null) {
                if (c.lastAck) {
                    Sender.connections.remove(address);
                    System.out.println("Disconnecting from " + address);
                } else if (ackChunk > c.getAckChunk() && (ackChunk - c.getAckChunk()) == 1){
                    // System.out.println("Router accecpt " + ackChunk + " to " + c.getAckChunk());

                    c.setAckChunk(ackChunk);
                } else{
                    // System.out.println("Router reject " + ackChunk + "/" + c.getAckChunk());
                }
            }
        }
    }
}
