package receiver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by elby on 7/25/16.
 */
public class Receiver {

    public static boolean initAck = false;
    public static ArrayList<SchemaEntry> entries;
    static long chunk = 0;
    public static long ackChunk = 0;
    public static int prob = 0;
    public static InetAddress senderAddress;

    public static void main(String args[]) throws IOException, InterruptedException {
        prob = Integer.parseInt(args[0]);
        String schema;
        entries = new ArrayList<SchemaEntry>();
        MulticastSocket schemaSocket = new MulticastSocket(6669); //listens for packts on 6669
        InetAddress group = InetAddress.getByName("225.226.227.228");
        schemaSocket.joinGroup(group);

        byte[] bytes = new byte[500];
        DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
        System.out.println("Listening on port 6669");

        schemaSocket.receive(packet);
        schema = new String(bytes);
        parseSchema(schema);
        System.out.println(schema);
        System.out.println("Receiving from " + senderAddress.getHostName());
        schemaSocket.close();

        DatagramSocket receiveSocket = new DatagramSocket(6669);

        Thread acker = new Thread(new Acker(ackChunk));
        acker.start();

        for(SchemaEntry sc : entries){
            System.out.println("Receiving file: " + sc.file);
            File file = new File(sc.file);
            FileOutputStream out = new FileOutputStream(file);

            if (!file.exists()) {
                file.createNewFile();
            }

            while(chunk <= sc.chunks){
                System.out.println("Chunk: " + chunk + "/" + sc.chunks);
                byte[] buffer;

                if(chunk == sc.chunks) buffer = new byte[sc.lastChunk + 8]; //for long
                else buffer = new byte[256 + 8]; //for long

                packet = new DatagramPacket(buffer, buffer.length);


                do {
                    receiveSocket.receive(packet);
                }while(getPacketChunk(buffer) != chunk);

                acker.interrupt();
                acker.join();

                setAckChunk(chunk);
                chunk++;
                byte[] d = getPayload(buffer);

//                printBuffer(d);
                out.write(d);

                acker = new Thread(new Acker(ackChunk));
                acker.start();
            }
            chunk = 0;
            out.close();
        }
        Thread.sleep(1000); //Timeout of once second to ensure completion
        if(acker.isAlive()) acker.interrupt();

    }


    private static void parseSchema(String schema){
        StringTokenizer st = new StringTokenizer(schema, ";");

        int count = 0;
        String fn = "";
        long c = 0;
        int lc = 0;
        boolean first = true; 
        String token = "";
        while(st.hasMoreTokens()){
            token = st.nextToken();
            if(first){
                try{ senderAddress = InetAddress.getByName(token);} catch (Exception e){ e.printStackTrace();}
                token = st.nextToken();
                first = false;
            }
            if(count == 0){
                fn = token;
                count++;
            }else if(count == 1){
                c = Long.parseLong(token);
                count++;
            }else if(count == 2){
                lc = Integer.parseInt(token);
                SchemaEntry se = new SchemaEntry(fn, c, lc);
                entries.add(se);
                count = 0;
            }
        }
    }

    public static synchronized long getCurrentChunk(){
        return chunk;
    }

    private static byte[] getPayload(byte[] buffer){

        byte[] data = new byte[buffer.length - 8];
//        System.out.println(data.length);
        for(int i = 0; i < data.length; i++){
            data[i] = buffer[i + 8];
        }

        return data;
    }

    //Sequence number is the first 8 bytes
    private static long getPacketChunk(byte[] buffer){
        byte[] bChunk = new byte[8];
        for(int i = 0; i < 8; i++){
            bChunk[i] = buffer[i];
        }

        return SchemaEntry.bytesToLong(bChunk);
    }

    static void printBuffer(byte[] bytes){
        for(int i = 0; i < bytes.length; i++){
            System.out.print(bytes[i] + ",");
        }
        System.out.println();
    }
    public static synchronized void setAckChunk(long l){
        ackChunk = l;
    }

    public static synchronized long getAckChunk(){
        return ackChunk;
    }
}
