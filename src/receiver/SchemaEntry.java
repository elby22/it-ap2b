package receiver;

import java.nio.ByteBuffer;

public class SchemaEntry {
    //Total packets necessary = chunks + 1 (for last chunk);
    public long chunks;
    public int lastChunk;
    public String file;

    public SchemaEntry(String fn, long c, int lc){
        this.file = fn;
        this.chunks = c;
        this.lastChunk = lc;
    }

    @Override
    public String toString() {
        return file + ";" + chunks + ";" + lastChunk +";";
    }

//    public static byteToString/
    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes);
        buffer.flip();
        return buffer.getLong();
    }

    static public byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(x);
        return buffer.array();
    }

    static public boolean dropPacket(int prob){
        double rand = Math.random() * 100;

        if(prob >= rand){
//            System.out.println("Oops, dropped a packet");
            return true;
        }
        else return false;
    }
}
