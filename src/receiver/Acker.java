package receiver;

import javax.xml.validation.Schema;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;

/**
 * Created by elby on 7/30/16.
 */
public class Acker implements Runnable{


    DatagramPacket ack;
    DatagramSocket ackSender;
    private long ackChunk;

    @Override
    public void run() {
        try {
            ackSender = new DatagramSocket(6670);
            byte[] bytes;

            if(!Receiver.initAck) {

                //Make persistant
                bytes = ackbuilder(-1);
                System.out.println("Schema ack sent.");
                ack = new DatagramPacket(bytes, 12, Receiver.senderAddress, 6668); //sends acks to 6668
                ackSender.send(ack);
                Receiver.initAck = true;
                ackSender.close();

            }else{
                bytes = ackbuilder(ackChunk);
                ack = new DatagramPacket(bytes, 12, Receiver.senderAddress, 6668);
                // System.out.println("Acking " + String.valueOf(ackChunk));

                while(!Thread.interrupted()){

                    if(!SchemaEntry.dropPacket(Receiver.prob))
                        ackSender.send(ack);

                    Thread.sleep(1);

                }
                ackSender.close();
            }


        } catch (InterruptedException e) {
            ackSender.close();
            Thread.currentThread().interrupt();
            return;
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private byte[] ackbuilder(long chunk) throws Exception{
        byte[] ack = new byte[12];

        byte[] address = InetAddress.getLocalHost().getAddress();
        byte[] payload = SchemaEntry.longToBytes(chunk);

        for(int i = 0; i < 4; i++){
            ack[i] = address[i];
        }

        for(int i = 4; i < 12; i++){
            ack[i] = payload[i - 4];
        }

        return ack;
    }

    public Acker(long l){
        this.ackChunk = l;
    }
}
