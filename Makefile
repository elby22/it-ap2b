default: sender receiver

sender: src/sender/Sender.java src/sender/Connection.java src/sender/Listener.java src/sender/Router.java src/receiver/SchemaEntry.java
	javac -g -d . src/sender/*.java src/receiver/SchemaEntry.java
	jar cmvf src/sender/META-INF/MANIFEST.MF Sender.jar sender/*.class receiver/SchemaEntry.class
	rm -r sender/ receiver/

receiver: src/receiver/Acker.java src/receiver/SchemaEntry.java src/receiver/SchemaEntry.java
	javac -g -d . src/receiver/*.java
	jar cmvf src/receiver/META-INF/MANIFEST.MF Receiver.jar receiver/*.class
	rm -r receiver/

clean:
	rm *.jar